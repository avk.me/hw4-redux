import {
  DOWNLOADED_CARDS_FROM_CART,
  ADDED_TO_CART,
  REMOVED_FROM_CART,
} from "../actions/cart.actions";

const initialState = {
  productsToCartId: localStorage.getItem("cart")?.split(",") || [],
  productsToCart: [],
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case DOWNLOADED_CARDS_FROM_CART:
      return {
        ...state,
        productsToCart: action.payload,
      };

    case ADDED_TO_CART:
      return {
        ...state,
        productsToCartId: action.payload,
      };

    case REMOVED_FROM_CART:
      return {
        ...state,
        productsToCartId: action.payload,
      };

    default:
      return state;
  }
};

export default cartReducer;
